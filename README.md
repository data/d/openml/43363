# OpenML dataset: All-of-Trumps-Tweets-(2009-2020)

https://www.openml.org/d/43363

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Content
The data includes the text, whether the tweet is a retweet, whether the tweet is deleted, and so much more. It is sorted by descending date (so the highest rows are from 2009 and the last rows are from 2020).
Acknowledgements
The data was retrieved from the Trump Twitter Archive, linked here: https://www.thetrumparchive.com/faq
Inspiration
Could be used for Sentiment analysis

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43363) of an [OpenML dataset](https://www.openml.org/d/43363). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43363/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43363/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43363/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

